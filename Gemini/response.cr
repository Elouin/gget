module Gemini
  # represents an Gemini Response
  class Response

    getter header : String
    property body : IO | String | Nil

    def initialize(header : String, body : IO | String | Nil)
      @header = header
      @body = body
    end

  end
end
