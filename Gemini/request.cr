module Gemini
  # a gemini request
  class Request
    property url : URI

    def initialize(url : String | URI)
      if url.is_a?(String) && !url.starts_with?("gemini://") || url.is_a?(URI) && !url.scheme == "gemini"
        STDERR.puts "ERROR: didn't provide a valid URL!"
        exit(1)
      end
      if url.is_a?(String)
        @url = URI.parse(url)
      else
        @url = url
      end
    end
    
    # send a request to a server and return a response
    def send
      if @url.port.nil?
        @url.port = 1965
      end
      context = OpenSSL::SSL::Context::Client.new
      context.verify_mode =  OpenSSL::SSL::VerifyMode::NONE
      conn = OpenSSL::SSL::Socket::Client.new(TCPSocket.new(@url.host.to_s, @url.port), hostname: @url.host.to_s, context: context)
      conn << "#{@url}\r\n"
      conn.flush
      resp = Gemini::Response.new(conn.gets.to_s, "")
      unless resp.header.starts_with?("20 ")
        STDERR.puts "ERROR: request wasn't successful!"
        exit(1)
      end
      resp.body = conn
      return resp
    end
  end
end

