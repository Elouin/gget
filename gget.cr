# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "option_parser"
require "socket"
require "openssl"
require "uri"
require "./Gemini/*"

port = 1965
host : String = ""

parser = OptionParser.new do |parser|
  parser.banner = "Usage: gget <url> [arguments]"
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit(1)
  end
end

parser.parse

uri = URI.parse(ARGV[0])

targeted_file = uri.path.split('/')[-1]
if targeted_file == ""
  STDERR.puts "ERROR: please provide a direct link to a file."
  exit(1)
end

repl = Gemini::Request.new(uri).send
if repl.body
  File.write(uri.path.split('/')[-1], repl.body)
end

