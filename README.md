# gget - gemini download utility

Download files via gemini. Currently only works with direct links to files and not links to directories.

```
Usage: gget <url> [arguments]
```

## Building/Installation

`shards build`

Otherwise, binaries are provided with the releases.

### Static Linking

Because of problems with the OpenSSL library, when trying to build a statically linked executable, i use Docker as mentioned in the documentation.

`docker run --rm -it -v $(pwd):/workspace -w /workspace crystallang/crystal:latest-alpine shards build --production --static`

## Contributing

1. Fork it (<https://codeberg.org/repo/fork/30006>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
